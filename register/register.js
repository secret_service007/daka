const app = getApp();
const db = wx.cloud.database();
const admin = db.collection('user');

Page({
  data: {
    name: '',
    zhanghao: '',
    mima: ''
  },
  //获取用户名
  getName(event) {
    console.log('获取输入的用户名', event.detail.value)
    this.setData({
      name: event.detail.value
    })
  },
  //获取用户账号
  getZhangHao(event) {
    console.log('获取输入的账号', event.detail.value)
    this.setData({
      zhanghao: event.detail.value
    })
  },
  // 获取密码
  getMiMa(event) {
    console.log('获取输入的密码', event.detail.value)
    this.setData({
      mima: event.detail.value
    })
  },
  

  onLoad: function() {
    if (!wx.cloud) {
      wx.redirectTo({
        url: '../chooseLib/chooseLib',
      })
      return
    }

   
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo
              })
            }
          })
        }
      }
    })
  },

  onGetUserInfo: function(e) {
    if (!this.data.logged && e.detail.userInfo) {
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo
      })
    }
  },

  onGetOpenid: function() {
 
    wx.cloud.callFunction({
      name: 'login',
      data: {},
      success: res => {
        console.log('[云函数] [login] user openid: ', res.result.openid)
        app.globalData.openid = res.result.openid
        wx.navigateTo({
          url: '../userConsole/userConsole',
        })
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
        wx.navigateTo({
          url: '../deployFunctions/deployFunctions',
        })
      }
    })
  },



    //注册
zhuce(){
  console.log('点击了注册');
  let that = this;
    let flag = false  //是否存在 true为存在
    let name = this.data.name
  let zhanghao = this.data.zhanghao
  let mima = this.data.mima
  console.log("点击了注册")
  console.log("name", name)
  console.log("zhanghao", zhanghao)
  console.log("mima", mima)
  //校验用户名
  if (name.length < 2) {
    wx.showToast({
      icon: 'none',
      title: '用户名至少2位',
    })
    return
  }
  if (name.length > 10) {
    wx.showToast({
      icon: 'none',
      title: '用户名最多10位',
    })
    return
  }
  //校验账号
  if (zhanghao.length < 4) {
    wx.showToast({
      icon: 'none',
      title: '账号至少4位',
    })
    return
  }

  //校验密码
  if (mima.length < 4) {
    wx.showToast({
      icon: 'none',
      title: '密码至少4位',
    })
    return
  }
    //查询用户是否已经注册
    admin.get({
      success: (res) => {
        let admins = res.data;  //获取到的对象数组数据
        console.log('获取到的对象数组数据');
         console.log(admins);
        for (let i = 0; i < admins.length; i++) {  //遍历数据库对象集合
          console.log('开始遍历');
          console.log(this.data.zhanghao);
          if (this.data.zhanghao== admins[i].zhanghao) { //用户名存在
            console.log('账号存在');
            flag = true;
            break;
          }
        }
        console.log('遍历结束');
        if (flag == true) {  
          console.log('账号已注册');  //已注册
          wx.showToast({
            title: '账号已注册！',
            icon: 'success',
            duration: 2500
          })
        } else {  //未注册
          console.log('账号未注册');
          that.saveuserinfo()
        }
      }
    })
  },
  //注册用户信息
  saveuserinfo() {
    console.log('开始注册');
    let that = this;
    console.log(this);
    admin.add({  //添加数据
      data: {
        zhanghao: this.data.zhanghao,
        mima:this.data.mima,
        name:this.data.name
      }
    }).then(res => {
      console.log('注册成功！')
      wx.showToast({
        title: '注册成功！',
        icon: 'success',
        duration: 3000
      })
      // wx.redirectTo({
      //   url: '/pages/login/login',
      // })
    })
  }
})

//



 
  //   //校验账号是否重复
  // wx.cloud.database().collection('user').where({
  //   zhanghao:this.data.zhanghao
  // }).get({
  //   success(res){
  //     let user = res.data[0]
  //     console.log("user",user)
  //     if(zhanghao == user.zhanghao){
  //       console.log('检测到相同账号')
  //       wx.showToast({
  //         title: '账号已存在',
  //       })
  //     }
  //   }
    
  // })
  // //

  // wx.cloud.database().collection('user').add({
  //   data: {
  //     name: name,
  //     zhanghao: zhanghao,
  //     mima: mima
  //   },
  //   success(res) {
  //     console.log('注册成功', res)
  //     wx.showToast({
  //       title: '注册成功',
  //     })
  //     wx.navigateTo({
  //       url: '../login/login',
  //     })
  //   },
  //   fail(res) {
  //     console.log('注册失败', res)
  //   }
  // })





